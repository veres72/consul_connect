services {
  name = "client"
  port = 9080 
  check {
	  http = "http://localhost:9080/api/agent1b/state"
	  interval = "10s"
	}
  connect {sidecar_service {}}
}

services {
  // Описание сервиса
  name = "echo"
  port = 9090
  check {
	  http = "http://localhost:9090/api/agent1a/state"
	  interval = "10s"
	}
  // Регестририуем рядом с сервисом что в него можно ходить как в сайдкар
  connect {sidecar_service {}}
}

ports {
  grpc = 8502
}

enable_central_service_config = true

acl {
  enabled = false
}