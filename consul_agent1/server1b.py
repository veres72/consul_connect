# simple python server with one function with very simple check
import socketserver
import http.server
import re

PORT = 9080

class CustomHandler(http.server.SimpleHTTPRequestHandler):
   def do_GET(self):
      if None != re.search('/api/agent1b/state', self.path):
         self.send_response(200)
         self.send_header('Content-type','text/html')
         self.end_headers()
         self.wfile.write('I am server 1b\n'.encode()) # call sample function here
         return
      else:
          http.server.SimpleHTTPRequestHandler.do_GET(self)

httpd = socketserver.ThreadingTCPServer(('', PORT),CustomHandler)

print("serving at port", PORT)
httpd.serve_forever()