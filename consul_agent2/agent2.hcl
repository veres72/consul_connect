services {
  name = "agent2"
  port = 7080 
  check {
	  http = "http://localhost:7080/api/agent2/state"
	  interval = "10s"
	}
  // Описание сайдкаров, работающих для этого сервиса.
  // Тут можно перечилсять гору апстримов которые будут доступны сайдкарами 
  // как прокси с энвоем
  connect {
    sidecar_service {
      proxy {
        upstreams {
          destination_name = "echo"
          local_bind_port = 7191
        },
        upstreams {
          destination_name = "client"
          local_bind_port = 7181
        }
      }
    }
  }
}

ports {
  grpc = 8502
}

enable_central_service_config = true

acl {
  enabled = false
}