FROM consul:consul:1.5.2
FROM envoyproxy/envoy:v1.11.0
COPY --from=0 /bin/consul /bin/consul
ENTRYPOINT ["dumb-init", "consul", "connect", "envoy"]