connect {
  enabled = true
}

ports {
  grpc = 8502
}

enable_central_service_config = true

acl {
  enabled = false
}